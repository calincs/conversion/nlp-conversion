# NLP-conversion

Code to move from Diffbot NLP-API output that we get through the LINCS-API, to LINCS RDF using CIDOC CRM and the web-annotation data model.

Mapping diagrams can be found here: https://drive.google.com/drive/u/0/folders/1s3mwynGfBREtJQfUIWjXB5GeKMxHOGJR

## Running the Flask Application

`docker build -t lincs-nlp .`

`docker run -p 8012:8012 lincs-nlp`

Now you can access it at `http://0.0.0.0:8012/extract` or `http://127.0.0.1:8012/extract` depending on your machine.