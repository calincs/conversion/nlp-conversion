import requests
import json


def nlp_api_request(file_content: str, TOKEN: str):
    # Diffbot NLP API request

    FIELDS = "entities,facts"
    HOST = "nl.diffbot.com"
    payload = {
    "content": file_content,
    "lang": "en",
    "format": "plain text"}

    res = requests.post("https://{}/v1/?fields={}&token={}".format(HOST, FIELDS, TOKEN), json=payload)
    ret = None
    try:
        ret = res.json()
    except Exception as err:
        print("Bad response: " + res.text)
        print(res.status_code)
        print(res.headers)
        print(err)
        return res.text.encode('utf-8')

    # Return the response json as bytes
    return json.dumps(ret).encode('utf-8')
