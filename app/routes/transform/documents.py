from rdflib import Namespace, URIRef, Literal
from rdflib.namespace import RDFS, RDF, XSD
from app.routes.transform.utils.prefixes import *
from app.routes.transform.utils.utils import text_after_last_period, text_up_to_next_period


class Document():
	def __init__(self, name: str, uri: str, lang: str):
		self.text: str = None
		self.name: str = name
		self.uri: str = uri
		self.lang: str = lang

	def set_text_from_string(self, file_contents: str, encoding="utf-8"):
		self.text = file_contents

	def set_text_from_file(self, file_path: str, encoding="utf-8"):
		with open(file_path, "r", encoding=encoding) as f_in:
			self.text = f_in.read()

	def declare_rdf(self, g):
		# document is D1 and has rdfs label
		g.add(URIRef(self.uri), RDF.type, URIRef(crmdig("D1_Digital_Object")))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.name, lang=self.lang))

		# document has a title
		title_uri = URIRef(temp_lincs(f"{self.uri}/title/{self.lang.lower()}"))
		g.add(URIRef(self.uri), URIRef(cidoc("P1_is_identified_by")), title_uri)
		g.add(title_uri, RDF.type, URIRef(cidoc("E33_E41_Linguistic_Appellation")))
		g.add(title_uri, URIRef(cidoc("P190_has_symbolic_content")), Literal(self.name, lang=self.lang))
		g.add(title_uri, RDFS.label, Literal("Title of " + self.name, lang="en"))
		g.add(title_uri, RDFS.label, Literal("Titre de " + self.name, lang="fr"))

		# document has a language
		E33_uri = URIRef(temp_lincs(f"{self.uri}/text/{self.lang.lower()}"))
		if self.lang == "fr":

			g.add(URIRef(self.uri), URIRef(cidoc("P106_is_composed_of")), E33_uri)

			g.add(E33_uri, RDF.type, URIRef(cidoc("E33_Linguistic_Object")))
			g.add(E33_uri, RDFS.label, Literal(f"French text of {self.name}", lang="en"))
			g.add(E33_uri, RDFS.label, Literal(f"Texte français de {self.name}", lang="fr"))

			g.add(E33_uri, URIRef(cidoc("P72_has_language")), URIRef("http://www.lexvo.org/id/iso639-3/fra"))

			g.add(URIRef("http://www.lexvo.org/id/iso639-3/fra"), RDF.type, URIRef(cidoc("E56_language")))
			g.add(URIRef("http://www.lexvo.org/id/iso639-3/fra"), RDFS.label, Literal("French", lang="en"))
			g.add(URIRef("http://www.lexvo.org/id/iso639-3/fra"), RDFS.label, Literal("Français", lang="fr"))

		elif self.lang == "en":
			g.add(URIRef(self.uri), URIRef(cidoc("P106_is_composed_of")), E33_uri)

			g.add(E33_uri, RDF.type, URIRef(cidoc("E33_Linguistic_Object")))
			g.add(E33_uri, RDFS.label, Literal(f"English text of {self.name}", lang="en"))
			g.add(E33_uri, RDFS.label, Literal(f"Texte anglais de {self.name}", lang="fr"))

			g.add(E33_uri, URIRef(cidoc("P72_has_language")), URIRef("http://www.lexvo.org/id/iso639-3/eng"))

			g.add(URIRef("http://www.lexvo.org/id/iso639-3/eng"), RDF.type, URIRef(cidoc("E56_language")))
			g.add(URIRef("http://www.lexvo.org/id/iso639-3/eng"), RDFS.label, Literal("English", lang="en"))
			g.add(URIRef("http://www.lexvo.org/id/iso639-3/eng"), RDFS.label, Literal("Anglais", lang="fr"))


class Mention():
	def __init__(self, mention_dict, document: Document):
		self.text: str = mention_dict["text"]
		self.start_offset: int = mention_dict["beginOffset"]
		self.end_offset: int = mention_dict["endOffset"]
		self.confidence: float = mention_dict["confidence"]
		self.document = document

		# TODO define right and left context
		# if the mention is also in a fact, then use that passage.
		# otherwise extract it from the document
		self.left_context = text_after_last_period(self.document.text[:self.start_offset])
		self.right_context = text_up_to_next_period(self.document.text[self.end_offset:])
		self.passage = self.left_context + self.text + self.right_context
