from rdflib import URIRef, Literal
from rdflib.namespace import RDFS, RDF, XSD

from app.routes.transform.utils.prefixes import *
from app.routes.transform.documents import Document, Mention
from app.routes.transform.entities import Entity
from app.routes.transform.events import Event


class Annotation():
	def __init__(self, document: Document, entity: Entity, mention: Mention):
		self.entity = entity
		self.document = document
		self.mention = mention
		self.uri = temp_lincs(f"{self.document.uri}/{self.entity.uri}/annotation/{self.mention.text}/{self.mention.start_offset}_{self.mention.end_offset}")
		self.en_label = f"Annotation about {self.entity.name} in {self.document.name}"
		self.fr_label = f"Annotation à propos de {self.entity.name} dans {self.document.name}"

	def declare_rdf(self, g):
		# we declare a new annotation for each mention

		# make a single annotation with a french label and and english label
		# that annotation has motivatedBy oa:identifying which is an E55_Type
		# that annotation hasBody to that person mentioned
		# that's all for the main annotation function
		# the rest will be in the target function
		# check that each language has a mention otherwise don't use it as a P67

		# doc oa:hasBody the entity mentioned
		# that entity has already been declared so we only connect here
		g.add(URIRef(self.uri), URIRef(oa("hasBody")), URIRef(self.entity.uri))

		# the annotation has WA type annotation
		g.add(URIRef(self.uri), RDF.type, URIRef(oa("Annotation")))

		# the annotation has labels
		g.add(URIRef(self.uri), RDFS.label, Literal(self.en_label, lang="en"))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.fr_label, lang="fr"))

		# the annotation has motivatedBy
		g.add(URIRef(self.uri), URIRef(oa("motivatedBy")), URIRef(oa("identifying")))
		g.add(URIRef(oa("identifying")), RDF.type, URIRef(cidoc("E55_Type")))
		g.add(URIRef(oa("identifying")), RDFS.label, Literal("identifying", lang="en"))


# TODO make classes for the textquoteselectors
class Target():
	def __init__(self, annotation: Annotation, document: Document, entity: Entity, mention: Mention):

		self.annotation = annotation
		self.document = document
		self.entity = entity
		self.mention = mention

		self.uri = temp_lincs(f"{self.document.uri}/{self.entity.uri}/specificresource/{self.mention.text}/{self.mention.start_offset}_{self.mention.end_offset}")
		self.selector_uri = temp_lincs(f"{self.document.uri}/{self.entity.uri}/textquotefull/{self.mention.text}/{self.mention.start_offset}_{self.mention.end_offset}")
		self.selector_specific_uri = temp_lincs(f"{self.document.uri}/{self.entity.uri}/textquotespecific/{self.mention.text}/{self.mention.start_offset}_{self.mention.end_offset}")

		self.target_label_en = f"{self.entity.name} mentioned in {self.document.name}"
		self.target_label_fr = f"{self.entity.name} mentionnée dans {self.document.name}"
		self.textquote_label_en = f"{self.entity.name} text selector in {self.document.name}"
		self.textquote_label_fr = f"Sélecteur de texte pour mention de {self.entity.name} dans {self.document.name}"

	def declare_rdf(self, g):

		# the annotation has a target
		g.add(URIRef(self.annotation.uri), URIRef(oa("hasTarget")), URIRef(self.uri))

		# the target refers to the body
		g.add(URIRef(self.uri), URIRef(cidoc("P67_refers_to")), URIRef(self.entity.uri))

		# the target has types and labels
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E73_Information_Object")))
		g.add(URIRef(self.uri), RDF.type, URIRef(oa("SpecificResource")))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.target_label_en, lang="en"))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.target_label_fr, lang="fr"))

		# the target has a source
		g.add(URIRef(self.uri), URIRef(oa("hasSource")), URIRef(self.document.uri))

		# the document is composed of the target
		g.add(URIRef(self.document.uri), URIRef(cidoc("P106_is_composed_of")), URIRef(self.uri),)

		# the target has symbolic content of the entity mention text
		g.add(URIRef(self.uri), URIRef(cidoc("P190_has_symbolic_content")), Literal(self.mention.text, lang=self.document.lang))

		# the target has a selector
		g.add(URIRef(self.uri), URIRef(oa("hasSelector")), URIRef(self.selector_uri))

		# the selector with types and labels
		g.add(URIRef(self.selector_uri), RDFS.label, Literal(self.textquote_label_en, lang="en"))
		g.add(URIRef(self.selector_uri), RDFS.label, Literal(self.textquote_label_fr, lang="fr"))
		g.add(URIRef(self.selector_uri), RDF.type, URIRef(oa("TextQuoteSelector")))

		# the selector has prefix, suffix, exact
		g.add(URIRef(self.selector_uri), URIRef(oa("prefix")), Literal(self.mention.left_context, lang=self.document.lang))
		g.add(URIRef(self.selector_uri), URIRef(oa("exact")), Literal(self.mention.text, lang=self.document.lang))
		g.add(URIRef(self.selector_uri), URIRef(oa("suffix")), Literal(self.mention.right_context, lang=self.document.lang))


		## the old entity model had two layers of text quote selectors.
		## Keeping this code temporarily in case the relation model add this back

		# # the selector has an oa:exact for the full text excerpt
		# g.add(URIRef(self.selector_uri), URIRef(oa("exact")), Literal(self.mention.passage, lang=self.document.lang))

		# # the selector is refined by a more specific selector
		# g.add(URIRef(self.selector_uri), URIRef(oa("refinedBy")), URIRef(self.selector_specific_uri))

		# # the more specific selector with types and labels
		# g.add(URIRef(self.selector_specific_uri), RDFS.label, Literal(self.textquote_label_en, lang="en"))
		# g.add(URIRef(self.selector_specific_uri), RDFS.label, Literal(self.textquote_label_fr, lang="fr"))
		# g.add(URIRef(self.selector_specific_uri), RDF.type, URIRef(cidoc("E33_Linguistic_Object")))
		# g.add(URIRef(self.selector_specific_uri), RDF.type, URIRef(oa("TextQuoteSelector")))


