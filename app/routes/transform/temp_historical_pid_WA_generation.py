from bs4 import BeautifulSoup
import rdflib
from rdflib import Namespace, URIRef, Literal
from rdflib.namespace import RDFS, RDF, XSD
import json
import os
import requests


"""

TODO
- there are some french DCB articles that have E in the URL the the F one is invalid
- if the mention is "John's" then sometimes they tag that as two separate mentions one for just the 's

- see if encoding needs fixing or if shows fine in RS
- when mention is empty
- when full context is empty
- should each language of title for E33_E41 be it's own instance? or two literals for one E33_E41?
- for now I just used context in the same paragraph after the preceding period and before the next one. That isn't a very precice method. I could do some NLP to get actual sentences or we can do a set number of words on each side.


* add french and english to titles of biographies
* fix exact for the specific text selector



DONE
- there are invalid mentions like campbell_john_1731_1795_4 in montgomery_richard_4 should be campbell_john_1731_95_4
- if only one language mentions the text then we can still say that both docs point to the annotation but then only have one target?
	- actually don't do this but bring it up as a question
- some mention context isn't being grabbed
['finlay_hugh_5', 'young_john_1819_5', '', 'Young', '']
['finlay_hugh_5', 'blake_charles_5', '', 'Blake', '']
['finlay_hugh_5', 'grant_william_1744_1805_5', '', 'Grant', '']
['finlay_hugh_5', 'inglis_charles_5', '', 'Inglis', '']
['finlay_hugh_5', 'edward_augustus_5', '', 'Edward Augustus', '']


- DCB names need to be cleaned up. or at least the ones i grab here

parsing
- if the mention is a * then choose the word before it
- fix text encoding so the spaces work
- get the context when it's not being grabbed
get the mention text when it's not being grabbed
- run the code for all the docs and upload it

"""


def iterate_in_batches(lst, batch_size):
	for i in range(0, len(lst), batch_size):
		yield lst[i:i + batch_size]


def text_after_last_period(text):
	# Find the last period in the text
	last_period_index = text.rfind('.')

	# If no period is found, return the whole string
	if last_period_index == -1:
		return text

	# If the last period is at the end of the text, return an empty string
	if last_period_index == len(text) - 1:
		return ""

	# Extract and return the text after the last period
	return text[last_period_index + 1:]


def text_up_to_next_period(text):
	# Split the text by periods
	sentences = text.split('.')

	# Filter out any empty strings resulting from periods at the end of the text
	sentences = [sentence.strip() for sentence in sentences if sentence.strip()]

	if sentences == []:
		return text

	return sentences[0]


def remove_leading_space(text):
	# Find the index of the first non-space and non-newline character
	index = 0
	while index < len(text) and (text[index] == ' ' or text[index] == '\n'):
		index += 1
	# Return the string starting from the first non-space and non-newline character
	return text[index:]


def remove_trailing_space(text):
	# Find the index of the last non-space and non-newline character
	index = len(text) - 1
	while index >= 0 and (text[index] == ' ' or text[index] == '\n'):
		index -= 1
	# Return the string up to (but not including) the last non-space and non-newline character
	return text[:index + 1]


def normalize_mention_lists(l1_full, l2_full):
	# l1_in = ['bochart_de_champigny_jean_2', 'gaillard_mathieu_1', 'auger_de_subercase_daniel_d_2', 'laumet_antoine_2', 'calliere_louis_hector_de_2', 'buade_de_frontenac_et_de_palluau_louis_de_1', 'le_roy_de_la_potherie_claude_charles_2', 'clairambault_d_aigremont_francois_2', 'raimbault_pierre_2']
	# l2_in = ['bochart_de_champigny_jean_2', 'gaillard_mathieu_1', 'auger_de_subercase_daniel_d_2', 'calliere_louis_hector_de_2', 'buade_louis_de_1', 'le_roy_de_la_potherie_claude_charles_2', 'raimbault_pierre_2']

	# l1_in = ['chewett_james_grant_9', 'chewett_william_7', 'copp_william_walter_12', 'scobie_hugh_8', 'nordheimer_abraham_9', 'gzowski_casimir_stanislaus_12']
	# l2_in = ['chewett_james_grant_9', 'chewett_william_7', 'copp_william_walter_12', 'maclear_thomas_12', 'scobie_hugh_8', 'nordheimer_abraham_9', 'gzowski_casimir_stanislaus_12']

	l1_in = [sublist[1] for sublist in l1_full]
	l2_in = [sublist[1] for sublist in l2_full]

	# l1_in = ["a", "b", "c"]
	# l2_in = ["a", "c", "c", "b", "d", "e"]

	l1_out = []
	l2_out = []

	# while l1_in and l2_in are not empty
	while len(l1_in) > 0 or len(l2_in) > 0:
		# if one list is empty and the other one isn't then we know to add empty strings to the one list
		if len(l1_in) == 0:
			l2_out.append(l2_full[0])
			l2_in.pop(0)
			l2_full.pop(0)
			l1_out.append([])
		elif len(l2_in) == 0:
			l1_out.append(l1_full[0])
			l1_in.pop(0)
			l1_full.pop(0)
			l2_out.append([])

		# if the mention matches then we continue
		elif l1_in[0] == l2_in[0]:
			l1_out.append(l1_full[0])
			l2_out.append(l2_full[0])
			l1_full.pop(0)
			l1_in.pop(0)
			l2_in.pop(0)
			l2_full.pop(0)

		# the current elements don't match
		elif l1_in[0] != l2_in[0]:
			# find the index of l1[0] in l2
			# find the index of l2[0] in l1
			# whichever is smaller index is the next element to add
			# and we fill in with blanks up to that point
			if l1_in[0] in l2_in:
				l1_in_l2 = l2_in.index(l1_in[0])
			else:
				l1_in_l2 = None

			if l2_in[0] in l1_in:
				l2_in_l1 = l1_in.index(l2_in[0])
			else:
				l2_in_l1 = None

			if l1_in_l2 is not None and l2_in_l1 is None:
				# l1[0] is still in l2 so we should save it
				# l2[0] is not in l1 so we can go ahead with it
				l1_out.append([])
				l2_out.append(l2_full[0])
				l2_in.pop(0)
				l2_full.pop(0)

			elif l2_in_l1 is not None and l1_in_l2 is None:
				# l2[0] is still in l1 so we should save it
				# l1[0] is not in l2 so we can go ahead with it
				l2_out.append([])
				l1_out.append(l1_full[0])
				l1_in.pop(0)
				l1_full.pop(0)

			elif l2_in_l1 is None and l1_in_l2 is None:
				# put them both in the other and add empties for them both
				l1_out.append(l1_full[0])
				l2_out.append([])

				l1_out.append([])
				l2_out.append(l2_full[0])

				l1_in.pop(0)
				l2_in.pop(0)
				l1_full.pop(0)
				l2_full.pop(0)

			# elif neither is none we have to go with minimum and only delete one.
			elif l2_in_l1 is not None and l1_in_l2 is not None:
				if l1_in_l2 < l2_in_l1:
					l1_out.append([])
					l2_out.append(l2_full[0])
					l2_in.pop(0)
					l2_full.pop(0)

				elif l1_in_l2 > l2_in_l1:
					l2_out.append([])
					l1_out.append(l1_full[0])
					l1_in.pop(0)
					l1_full.pop(0)

				# there is no way to know so we don't do a match
				elif l1_in_l2 == l2_in_l1:
					l1_out.append(l1_full[0])
					l2_out.append([])

					l1_out.append([])
					l2_out.append(l2_full[0])

					l1_in.pop(0)
					l2_in.pop(0)

					l1_full.pop(0)
					l2_full.pop(0)

	# for i in range(0, len(l1_out)):
	# 	print(l1_out[i])
	# 	print(l2_out[i])
	# 	print("_" * 10)

	return l1_out, l2_out


def lincs(text):
	return f"http://id.lincsproject.ca/{text}"


def cidoc(text):
	return f"http://www.cidoc-crm.org/cidoc-crm/{text}"


def crmdig(text):
	return f"http://www.ics.forth.gr/isl/CRMdig/{text}"


def temp_lincs(text):
	return f"http://temp.lincsproject.ca/{text}"


def oa(text):
	return f"http://www.w3.org/ns/oa#{text}"


class Person:
	def __init__(self, person_id, person_name, person_url):
		self.id = person_id
		self.name = person_name
		self.url = person_url


class People:
	def __init__(self):
		self.people_dict = {}
		self.id_swaps = {}

		self.wikidata_swap = {}
		with open("../postprocessing/wikidata_to_viaf.tsv", 'r') as f:
			for line in f:
				columns = line.strip().split('\t')
				wikidata_id = columns[0].replace('wikidata:', 'http://www.wikidata.org/entity/').strip()
				viaf_id = columns[1].replace('viaf:', 'http://viaf.org/viaf/').strip()
				self.wikidata_swap[wikidata_id] = viaf_id

	def load_from_tsv(self, file_path):
		# load the file of invalid ids
		with open("invalid_ids.txt", "r") as file:
			for line in file:
				old_id, new_id = line.strip().split('\t')
				self.id_swaps[old_id] = new_id

		with open(file_path, 'r') as file:
			for line in file:
				person_id, person_name, person_url = line.strip().split('\t')

				if "wikidata" in person_url:
					if person_url in self.wikidata_swap:
						person_url = self.wikidata_swap[person_url]

				person = Person(person_id, person_name, person_url)
				self.people_dict[person_id] = person

	def search_person_by_id(self, search_id):
		return self.people_dict.get(search_id)

	def swap_person_by_id(self, search_id):
		new_id = self.id_swaps.get(search_id)
		if new_id is None:
			return search_id
		return new_id


class Graph():
	def __init__(self):
		self.graph = None
		self.create_graph()

	def create_graph(self):
		self.graph = rdflib.Graph()

		self.graph.bind("crm", Namespace("http://www.cidoc-crm.org/cidoc-crm/"))
		self.graph.bind("temp_lincs", Namespace("http://temp.lincsproject.ca/"))
		self.graph.bind("frbroo", Namespace("http://iflastandards.info/ns/fr/frbr/frbroo/"))
		self.graph.bind("lincs", Namespace("http://www.id.lincsproject.ca/"))
		self.graph.bind("wikidata", Namespace("http://www.wikidata.org/entity/"))
		self.graph.bind("rdf", Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#"))
		self.graph.bind("rdfs", Namespace("http://www.w3.org/2000/01/rdf-schema#"))
		self.graph.bind("owl", Namespace("http://www.w3.org/2002/07/owl#"))
		self.graph.bind("skos", Namespace("http://www.w3.org/2004/02/skos/core#"))
		self.graph.bind("oa", Namespace("http://www.w3.org/ns/oa#"))
		self.graph.bind("geonames", Namespace("https://sws.geonames.org/"))
		self.graph.bind("lexvo", Namespace("http://www.lexvo.org/id/iso639-3/"))
		#self.graph.bind("dcb_bio_en", Namespace("http://www.biographi.ca/en/bio/"))
		#self.graph.bind("dcb_bio_fr", Namespace("http://www.biographi.ca/fr/bio/"))
		self.graph.bind("crmdig", Namespace("http://www.ics.forth.gr/isl/CRMdig/"))

	def add(self, subj, pred, obj):
		self.graph.add((subj, pred, obj))

	def serialize(self, output_location):
		self.graph.serialize(destination=output_location, format="turtle", encoding="utf-8", shorten_builtins=False)


class Document():
	def __init__(self, bio_subject, lang):
		self.lang: str = lang
		self.bio_id: str = bio_subject.id
		self.bio_name: str = bio_subject.name

		if lang == "english":
			self.uri: str = f"http://www.biographi.ca/en/bio/{self.bio_id}E.html"
			self.fr_title: str = f"Biographie en anglais du DCB de {self.bio_name}"
			self.en_title: str = f"English DCB biography of {self.bio_name}"
			self.lang_code: str = "en"
		elif lang == "french":
			self.uri: str = f"http://www.biographi.ca/fr/bio/{self.bio_id}F.html"
			self.fr_title: str = f"Biographie française du DCB de {self.bio_name}"
			self.en_title: str = f"French DCB biography of {self.bio_name}"
			self.lang_code: str = "fr"

	def declare_doc(self, g):
		# <http://www.biographi.ca/en/bio/ahchuchwahauhhatohapit_14E.html> a crmdig:D1_Digital_Object
		# and rdfs labels
		g.add(URIRef(self.uri), RDF.type, URIRef(crmdig("D1_Digital_Object")))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.en_title, lang="en"))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.fr_title, lang="fr"))


		# http://www.biographi.ca/en/bio/ahchuchwahauhhatohapit_14E.html> crm:P1_is_identified_by <title>
		# <title> a crm:E33_E41_Linguistic_Appellation
		# <title> crm:P190_has_symbolic_content "English DCB biography of {subject_name}"@en
		# <title> crm:P190_has_symbolic_content "French DCB biography of {subject_name}"@en
		# "Biographie en anglais du DCB de {subject_name}"@fr
		# "Biographie française du DCB de {subject_name}"@fr

		g.add(URIRef(self.uri), URIRef(cidoc("P1_is_identified_by")), URIRef(temp_lincs(f"{self.bio_id}/{self.lang_code}Bio/enTitle/")))
		g.add(URIRef(temp_lincs(f"{self.bio_id}/{self.lang_code}Bio/enTitle/")), RDF.type, URIRef(cidoc("E33_E41_Linguistic_Appellation")))
		g.add(URIRef(temp_lincs(f"{self.bio_id}/{self.lang_code}Bio/enTitle/")), URIRef(cidoc("P190_has_symbolic_content")), Literal(self.en_title, lang="en"))
		g.add(URIRef(temp_lincs(f"{self.bio_id}/{self.lang_code}Bio/enTitle/")), RDFS.label, Literal("Title of " + self.en_title, lang="en"))

		g.add(URIRef(self.uri), URIRef(cidoc("P1_is_identified_by")), URIRef(temp_lincs(f"{self.bio_id}/{self.lang_code}Bio/frTitle/")))
		g.add(URIRef(temp_lincs(f"{self.bio_id}/{self.lang_code}Bio/frTitle/")), RDF.type, URIRef(cidoc("E33_E41_Linguistic_Appellation")))
		g.add(URIRef(temp_lincs(f"{self.bio_id}/{self.lang_code}Bio/frTitle/")), URIRef(cidoc("P190_has_symbolic_content")), Literal(self.fr_title, lang="fr"))
		g.add(URIRef(temp_lincs(f"{self.bio_id}/{self.lang_code}Bio/frTitle/")), RDFS.label, Literal("Titre de " + self.fr_title, lang="fr"))


class Annotation():
	# TODO maybe grab the french mentioned person name too
	def __init__(self, fr_doc, en_doc, mentioned_person, fr_mention, en_mention, mention_index):
		self.fr_doc = fr_doc
		self.en_doc = en_doc
		self.mentioned_person = mentioned_person
		self.uri = f"http://temp.lincsproject.ca/dcb/{self.en_doc.bio_id}/annotation/{self.mentioned_person.id}/{mention_index}"
		self.en_label = f"Annotation about {self.mentioned_person.name} mentioned in the DCB biography of {self.en_doc.bio_name}"
		self.fr_label = f"Annotation à propos de {self.mentioned_person.name} mentionnée dans la biographie DBC de {self.fr_doc.bio_name}"

		if fr_mention == []:
			self.fr_mention = False
		else:
			self.fr_mention = True

		if en_mention == []:
			self.en_mention = False
		else:
			self.en_mention = True

	def declare_annotation(self, g):
		# we declare a new annotation for each mention but the annotation is shared between the french and english documents

		# make a single annotation with a french label and and english label
		# that annotation has motivatedBy oa:identifying which is an E55_Type
		# that annotation hasBody to that person mentioned
		# that's all for the main annotation function
		# the rest will be in the target function
		# check that each language has a mention otherwise don't use it as a P67

		# biography document id for each language -> P67_refers_to the mentioned person's id and that person id is a E21_Person
		# don't need to label that person since it should be in the rest of the DCB conversion

		if self.fr_mention:
			# french doc P67 mentioned person
			g.add(URIRef(self.fr_doc.uri), URIRef(cidoc("P67_refers_to")), URIRef(self.mentioned_person.url))
			g.add(URIRef(self.mentioned_person.url), RDF.type, URIRef(cidoc("E21_Person")))
			g.add(URIRef(self.mentioned_person.url), RDFS.label, Literal(self.mentioned_person.name, lang="en"))

		if self.en_mention:
			# english doc P67 mentioned person
			g.add(URIRef(self.en_doc.uri), URIRef(cidoc("P67_refers_to")), URIRef(self.mentioned_person.url))
			g.add(URIRef(self.mentioned_person.url), RDF.type, URIRef(cidoc("E21_Person")))
			g.add(URIRef(self.mentioned_person.url), RDFS.label, Literal(self.mentioned_person.name, lang="en"))

		# the annotation has types
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E33_Linguistic_Object")))
		g.add(URIRef(self.uri), RDF.type, URIRef(oa("Annotation")))

		# the annotation has labels
		g.add(URIRef(self.uri), RDFS.label, Literal(self.en_label, lang="en"))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.fr_label, lang="fr"))

		# the annotation has motivatedBy
		g.add(URIRef(self.uri), URIRef(oa("motivatedBy")), URIRef(oa("identifying")))
		g.add(URIRef(oa("identifying")), RDF.type, URIRef(cidoc("E55_Type")))
		g.add(URIRef(oa("identifying")), RDFS.label, Literal("identifying", lang="en"))

		# the annotation has the mentioned person as the body
		g.add(URIRef(self.uri), URIRef(oa("hasBody")), URIRef(self.mentioned_person.url))


class Target():
	def __init__(self, annotation, doc, mentioned_person, mention, mention_index, lang):
		# the mention might not have the right ID so use mentioned_person for it
		# because we swapped some IDs that were invalid
		self.annotation = annotation
		self.doc = doc
		self.mentioned_person = mentioned_person
		self.mention = mention
		self.mention_id = mention_index
		self.lang = lang
		self.uri = f"http://temp.lincsproject.ca/dcb/{self.doc.bio_id}/specificresource/{self.lang}/{self.mentioned_person.id}/{mention_index}"
		self.selector_uri = f"http://temp.lincsproject.ca/dcb/{self.doc.bio_id}/textquotefull/{self.lang}/{self.mentioned_person.id}/{mention_index}"
		self.selector_specific_uri = f"http://temp.lincsproject.ca/dcb/{self.doc.bio_id}/textquotespecific/{self.lang}/{self.mentioned_person.id}/{mention_index}"
		self.target_label_en = self.annotation.en_label.replace("Annotation about", "Annotation target about")
		self.target_label_fr = self.annotation.fr_label.replace("Annotation à", "Annotation cible à")
		self.textquote_label_en = self.annotation.en_label.replace("Annotation about", "Annotation text about")
		self.textquote_label_fr = self.annotation.fr_label.replace("Annotation à", "Annotation texte à")

	def declare_target(self, g):

		# the annotation has a target
		g.add(URIRef(self.annotation.uri), URIRef(oa("hasTarget")), URIRef(self.uri))

		# the target has types and labels
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E73_Information_Object")))
		g.add(URIRef(self.uri), RDF.type, URIRef(oa("SpecificResource")))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.target_label_en, lang="en"))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.target_label_fr, lang="fr"))

		# the target has a source
		g.add(URIRef(self.uri), URIRef(oa("hasSource")), URIRef(self.doc.uri))

		# the target has a selector
		g.add(URIRef(self.uri), URIRef(oa("hasSelector")), URIRef(self.selector_uri))

		# the selector with types and labels
		g.add(URIRef(self.selector_uri), RDFS.label, Literal(self.textquote_label_en, lang="en"))
		g.add(URIRef(self.selector_uri), RDFS.label, Literal(self.textquote_label_fr, lang="fr"))
		g.add(URIRef(self.selector_uri), RDF.type, URIRef(cidoc("E33_Linguistic_Object")))
		g.add(URIRef(self.selector_uri), RDF.type, URIRef(oa("TextQuoteSelector")))

		# the selector has an oa:exact for the full text excerpt
		g.add(URIRef(self.selector_uri), URIRef(oa("exact")), Literal(self.mention[2] + self.mention[3] + self.mention[4]))

		# the selector is refined by a more specific selector
		g.add(URIRef(self.selector_uri), URIRef(oa("refinedBy")), URIRef(self.selector_specific_uri))

		# the more specific selector with types and labels
		g.add(URIRef(self.selector_specific_uri), RDFS.label, Literal(self.textquote_label_en, lang="en"))
		g.add(URIRef(self.selector_specific_uri), RDFS.label, Literal(self.textquote_label_fr, lang="fr"))
		g.add(URIRef(self.selector_specific_uri), RDF.type, URIRef(cidoc("E33_Linguistic_Object")))
		g.add(URIRef(self.selector_specific_uri), RDF.type, URIRef(oa("TextQuoteSelector")))

		# the more specific selector has prefix, suffix, exact
		if self.mention[2] != "":
			g.add(URIRef(self.selector_specific_uri), URIRef(oa("prefix")), Literal(self.mention[2], lang=self.lang))
		if self.mention[3] != "":
			g.add(URIRef(self.selector_specific_uri), URIRef(oa("exact")), Literal(self.mention[3], lang=self.lang))
		if self.mention[4] != "":
			g.add(URIRef(self.selector_specific_uri), URIRef(oa("suffix")), Literal(self.mention[4], lang=self.lang))


def parse_html(filename):
	fr_mentions = []
	en_mentions = []
	for language in ["english", "french"]:
		folder = f"DCB_scraped/biographies/{language}/html/"
		with open(folder + filename + ".html", "r", encoding="utf-8") as f:
			html_content = f.read()

			# Parse the HTML content
			soup = BeautifulSoup(html_content, "html.parser")

			content_div = soup.find("section", id="first", class_="bio")

			# find all links in the HTML document and print their URLs
			if content_div is not None:
				links = content_div.find_all("a")
				if links is not None:
					for link in links:
						link_text = link.get("href")
						if link_text.startswith("/en/bio/") or link_text.startswith("/fr/bio/"):
							# Get the parent element of the <a> element
							parent = link.find_parent('p')

							# Extract the text context before and after the <a> element
							text_before = parent.text[:parent.text.index(link.text)].replace(" ", " ")
							text_after = parent.text[parent.text.index(link.text) + len(link.text):].replace(" ", " ")

							# only keep the sentence not the whole paragraph
							text_before = text_after_last_period(text_before)
							text_after = text_up_to_next_period(text_after)
							mention = link.text.replace(" ", " ")

							if mention.strip() == "*" or mention.strip() == "":
								#print()
								#print(mention)
								before_split = text_before.strip().split(" ")
								mention = before_split[-1]
								text_before = " ".join(before_split[:-1]) + " "
								#print(mention)

							mention = mention.replace("*", "")

							# Clean context
							left_context = remove_leading_space(text_before.replace("*", "").replace("  ", " "))
							right_context = remove_trailing_space(text_after.replace("*", "").replace("  ", " "))
							mentioned_link = link.get("href").replace("/en/bio/", "").replace("/fr/bio/", "").replace("E.html", "").replace("F.html", "")

							# it seems like when the mention text doesn't exist it's because there is a link to nothing on the page. so we ignore them and don't add annotations.
							if mention != "":
								if language == "english":
									en_mentions.append([filename, mentioned_link, left_context, mention, right_context])
								elif language == "french":
									fr_mentions.append([filename, mentioned_link, left_context, mention, right_context])
				else:
					print(f"ERROR with {filename}")
			else:
				print(f"ERROR with {filename}")

	en_mentions, fr_mentions = normalize_mention_lists(en_mentions, fr_mentions)

	return en_mentions, fr_mentions


def main():

	sample = False

	if sample:
		bio_file = "bio_id_list_sample.tsv"
	else:
		bio_file = "bio_id_list.tsv"

	# make a people class and load all those people into it
	# Create a People object and load data from the TSV file
	people_manager = People()
	people_manager.load_from_tsv("bio_id_list.tsv")

	# go through each bio_id and parse that biography and then convert each mention
	# do it in batches so it doesn't take as long to save the graphs and so the output files can be loaded into RS
	with open(bio_file, "r") as file:
		filenames = [line.strip().split("\t")[0] for line in file.readlines()]

		batch_size = 200
		batch_count = 0
		for batch in iterate_in_batches(filenames, batch_size):
			graph = Graph()

			for filename in batch:
				subject = people_manager.search_person_by_id(filename)
				en_mentions, fr_mentions = parse_html(filename)

				fr_doc = Document(subject, "french")
				en_doc = Document(subject, "english")

				fr_doc.declare_doc(graph)
				en_doc.declare_doc(graph)

				for i in range(0, len(en_mentions)):
					# create the shared annotation object between the two languages
					# though only attach it if there was a corresponding mention in the given language

					mentioned_id = None
					if en_mentions[i] != []:
						mentioned_id = en_mentions[i][1]
					if mentioned_id is None:
						if fr_mentions[i] != []:
							mentioned_id = fr_mentions[i][1]

					if mentioned_id:
						mentioned_person = people_manager.search_person_by_id(mentioned_id)

						if mentioned_person is None:
							mentioned_id = people_manager.swap_person_by_id(mentioned_id)
							mentioned_person = people_manager.search_person_by_id(mentioned_id)

						if mentioned_person:
							annotation = Annotation(fr_doc, en_doc, mentioned_person, fr_mentions[i], en_mentions[i], i)
							annotation.declare_annotation(graph)

							# now that we have the annotation object
							# we can create the target and all the connected mention info for each language separately

							if fr_mentions[i] != []:
								french_target = Target(annotation, fr_doc, mentioned_person, fr_mentions[i], i, "fr")
								french_target.declare_target(graph)

							if en_mentions[i] != []:
								english_target = Target(annotation, en_doc, mentioned_person, en_mentions[i], i, "en")
								english_target.declare_target(graph)

						else:
							print(f"skipping mention. can't find person {mentioned_id}")
							print(mentioned_person)
					else:
						print("skipping mention. no id.")
			print(f"serializing batch {batch_count}")
			output_directory = f"../data/web_annotations_output/dcb_webannotations_{batch_count}.ttl"
			graph.serialize(output_directory)
			batch_count += 1


main()
