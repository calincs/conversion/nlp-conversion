from rdflib import URIRef, Literal
from rdflib.namespace import RDFS, RDF, XSD

from app.routes.transform.utils.prefixes import *
from app.routes.transform.utils.utils import *
from app.routes.transform.graphs import Graph
from app.routes.transform.documents import Document


class Entity():
	def __init__(self):
		self.uri: str = None
		self.name: str = None
		self.labels: list = []
		self.entity_types: list = []

	# TODO add same as for additional URIs
	def set_uri(self, name: str, all_uris: list):
		# return a temp lincs URI or a Wikidata URI if it exists in all_uris
		self.uri = temp_lincs(f'{name.lower().replace(" ", "_")}')
		if all_uris != []:
			for link in all_uris:
				if "wikidata" in link:
					self.uri = link
					break

	def set_name(self, name: str):
		self.name = name

	def set_label(self, label: str):
		# TODO: should add handling for language
		self.labels.append(label)
		self.labels = list(set(self.labels))

	def add_types(self, types: list):
		self.entity_types = self.entity_types + types

	def set_label_from_mention(self, mention: dict):
		"""
		mention is dict of the form:
		{
			"text": "She",
			"beginOffset": 185,
			"endOffset": 188,
			"isPronoun": true,
			"confidence": 0.99993837
		}
		We don't want rdfs:label values of pronouns.
		"""
		if "isPronoun" not in mention:
			self.set_label(mention["text"])
		elif mention["isPronoun"] is False:
			self.set_label(mention["text"])

	def declare_type(self, g: Graph):
		for entity_type in self.entity_types:
			g.add(URIRef(self.uri), RDF.type, URIRef(entity_type))

	def declare_rdf(self, g: Graph, document: Document):
		self.declare_type(g)
		for label in self.labels:
			g.add(URIRef(self.uri), RDFS.label, Literal(label, lang=document.lang))

		# document refers to the entity
		g.add(URIRef(document.uri), URIRef(cidoc("P67_refers_to")), URIRef(self.uri))

		if "http://www.cidoc-crm.org/cidoc-crm/E21_Person" in self.entity_types:
			"""
			Define the person name type which we will keep as generic personal name for all instances:
				<http://id.lincsproject.ca/biography/personalName> a crm:E55_Type ;
					rdfs:label	“personal name”@en .

			"""
			g.add(URIRef(lincs("biography/personalName")), RDF.type, URIRef(cidoc("E55_Type")))
			g.add(URIRef(lincs("biography/personalName")), RDFS.label, Literal("personal name", lang="en"))

			"""
			For every name a person has, attach a P1_is_identified_by to the E21_Person:
				lincs:personID
				a                            crm:E21_Person ;
				rdfs:label                   "<personName>", “<alternativePersonName>” ;
				crm:P1_is_identified_by      temp_lincs:personID/name/<personName> .

			For every name, define as E33_E41:
				temp_lincs:personID/name/<personName>
				a                              crm:E33_E41_Linguistic_Appellation ;
				rdfs:label                     "Name of <personName>"@en ;
				crm:P190_has_symbolic_content  "<personName>"@en ;
				crm:P2_has_type                <http://id.lincsproject.ca/biography/personalName> .
			"""
			for name in self.labels:
				name_uri = temp_lincs(f'{self.uri}/name/{name.replace(" ", "_")}')
				g.add(URIRef(self.uri), URIRef(cidoc("P1_is_identified_by")), URIRef(name_uri))
				g.add(URIRef(name_uri), RDF.type, URIRef(cidoc("E33_E41_Linguistic_Appellation")))
				g.add(URIRef(name_uri), RDFS.label, Literal(f"Name of {name}", lang="en"))
				g.add(URIRef(name_uri), URIRef(cidoc("P190_has_symbolic_content")), Literal(name, lang="en"))
				g.add(URIRef(name_uri), URIRef(cidoc("P2_has_type")), URIRef(lincs("biography/personalName")))


# TODO need to fully declare the date and integrate it with facts that connect to dates.
# g.add(URIRef(timespan_uri), URIRef(cidoc("P82_at_some_time_within")), Literal(self.date, lang="en"))
# # get start and end dates from the date
# begin, end = get_begin_end_dates(self.date)
# begin = None  # TEMP
# end = None  # TEMP
# if begin and end:
# 	g.add(URIRef(timespan_uri), URIRef(cidoc("P82a_begin_of_the_begin")), Literal(begin, datatype=XSD.dateTime))
# 	g.add(URIRef(timespan_uri), URIRef(cidoc("P82b_end_of_the_end")), Literal(end, datatype=XSD.dateTime))