import json

from app.routes.transform.documents import Document, Mention
from app.routes.transform.entities import *
from app.routes.transform.events import *
from app.routes.transform.annotations import Annotation, Target
from app.routes.transform.graphs import Graph


def parse_entity(entity_dict, document, graph, entity_type_dict):
	# figures out the entity type and then directs it through the correct processing
	# entity dict is one entity dict from the list of entity dicts in the diffbot response

	entity = Entity()

	if "name" in entity_dict:
		entity.set_uri(entity_dict["name"], entity_dict["allUris"])
		entity.set_name(entity_dict["name"])
		entity.set_label(entity_dict["name"])  # TODO handle label language based on document
	if "mentions" in entity_dict:
		for mention in entity_dict["mentions"]:
			entity.set_label_from_mention(mention)  # TODO handle label language based on document

		# an entity has to be mentioned at least once
		for diffbot_type in entity_dict["allTypes"]:
			try:
				cidoc_types = entity_type_dict[diffbot_type["name"]]["rdf_type"]
				entity.add_types(cidoc_types)
			except KeyError:
				print(f"DIFFBOT TYPE ERROR: type {diffbot_type['name']} ({diffbot_type['diffbotUri']}) not handled in our mapping.")

	# TODO may need to force ignore some types. To see after adding relations.
	if entity.entity_types != []:
		entity.declare_rdf(graph, document)
		return entity
	else:
		return None


def parse_fact(fact_dict, document, graph):
	# handle fact subject

	# re-declare the entity in case it was not in the entities section
	parse_entity(fact_dict["entity"], document, graph)

	# create an Entity object so we can keep it's URI
	subj = Entity()
	subj.set_uri(fact_dict["entity"]["name"], fact_dict["entity"]["allUris"])
	subj.set_name(fact_dict["entity"]["name"])

	# get the object, declare RDF and also keep as object with name and URI
	parse_entity(fact_dict["value"], document, graph)
	obj = Entity()
	obj.set_uri(fact_dict["value"]["name"], fact_dict["value"]["allUris"])
	obj.set_name(fact_dict["value"]["name"])

	pred = fact_dict["property"]

	# get the full evidence
	passages = []
	subj_mentions = []
	obj_mentions = []
	for evidence in fact_dict["evidence"]:
		if "passage" in evidence:
			passages.append(evidence["passage"])
		if "entityMentions" in evidence:
			for entity_mention in evidence["entityMentions"]:
				if "text" in entity_mention:
					subj_mentions.append(entity_mention["text"])
		if "valueMentions" in evidence:
			for value_mention in evidence["valueMentions"]:
				if "text" in value_mention:
					obj_mentions.append(value_mention["text"])
	passages = list(set(passages))
	subj_mentions = list(set(subj_mentions))
	obj_mentions = list(set(obj_mentions))

	event = None

	# Birth
	if pred["name"] in ["place of birth", "date of birth"]:
		event = BirthEvent()
		event.set_subject(subj)

		if pred["name"] == "place of birth":
			event.set_place(obj)

		elif pred["name"] == "date of birth":
			event.set_date(fact_dict["value"]["name"])

		event.declare_birth(graph)

	# Death
	elif pred["name"] in ["place of death", "date of death", "cause of death"]:
		event = DeathEvent()
		event.set_subject(subj)

		if pred["name"] == "place of death":
			event.set_place(obj)

		elif pred["name"] == "date of death":
			event.set_date(fact_dict["value"]["name"])

		if pred["name"] == "cause of death":
			event.set_cause(obj)

		event.declare_death(graph)

	# TODO if it turns out there can be more than one relation from a single pred name then this will need to be declared more than once
	# and in that case the elif for each pred["name"] condition would need to become if
	# if event:
	# 	annotation = Annotation(subj, event, annotation_source, pred["name"])
	# 	annotation.declare_annotation(graph)

	# 	attribute = AttributionAssignment(subj, obj, event, annotation, pred["name"], passages + obj_mentions)
	# 	attribute.declare_attribution_assignment(graph)

	# 	for evidence in fact_dict["evidence"]:
	# 		# TODO gender doesn't have the same info so we need to figure out how to get evidence
	# 		fragment_obj_mentions = []
	# 		if "passage" in evidence:
	# 			passage = evidence["passage"]
	# 			if "valueMentions" in evidence:
	# 				for value_mention in evidence["valueMentions"]:
	# 					if "text" in value_mention:
	# 						fragment_obj_mentions.append([value_mention["text"], value_mention["beginOffset"], value_mention["endOffset"]])

	# 				segment_selector = SegmentSelector(document, passage, annotation_source)
	# 				segment_selector.declare_segment_selector(graph)

	# 				for fragment in fragment_obj_mentions:
	# 					fragment_selector = FragmentSelector(document, fragment[0], segment_selector, start=fragment[1], end=fragment[2])
	# 					fragment_selector.declare_fragment_selector(graph)


def transform_json(text_file_content: str, diffbot_dict: dict, document_name: str, document_uri: str, document_language: str, entities_only: bool = True):
	# One RDF graph for the whole document
	graph = Graph()

	# Define the document object once for this request
	document = Document(document_name, document_uri, document_language)
	document.set_text_from_string(text_file_content)
	document.declare_rdf(graph)

	with open("/code/app/routes/transform/config/entity_types.json", 'r') as file:
		entity_type_dict = json.load(file)

	for entity_dict in diffbot_dict["entities"]:
		entity = parse_entity(entity_dict, document, graph, entity_type_dict)

		if entity is not None:
			if "mentions" in entity_dict:
				for mention_dict in entity_dict["mentions"]:
					if "text" in mention_dict and "beginOffset" in mention_dict and "endOffset" in mention_dict:
						mention = Mention(mention_dict, document)
						annotation = Annotation(document, entity, mention)
						annotation.declare_rdf(graph)
						target = Target(annotation, document, entity, mention)
						target.declare_rdf(graph)
					else:
						print("SKIPPED A MENTION:")
						print(mention_dict)

	# TODO handle entities from facts that aren't in entities already
	# TODO process facts first, and track the entity mentions. Then any mentions not already covered can be added from the entities section.

	return graph.to_string()
