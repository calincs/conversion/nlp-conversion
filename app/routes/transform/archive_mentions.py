from rdflib import URIRef, Literal
from rdflib.namespace import RDFS, RDF, XSD

from app.routes.transform.utils.prefixes import *
from app.routes.transform.documents import Document
from app.routes.transform.entities import Entity
from app.routes.transform.events import Event


class AnnotationSource():
	def __init__(self, document: Document):
		self.uri: str = None  # TODO probably need to generate one from a document ID if there isn't one provided
		self.label: str = document.name
		self.source: str = document.uri

		self.set_uri()

	def set_uri(self):
		self.uri = temp_lincs(f'annotationSource/{self.label.replace(" ", "_")}')

	def declare_annotation_source(self, g):
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E73_Information_Object")))
		g.add(URIRef(self.uri), RDF.type, URIRef(oa("SpecificResource")))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.label, lang="en"))
		g.add(URIRef(self.uri), URIRef(oa("hasSource")), URIRef(self.source))
		# Annotation source is connected to segment selectors in the segment selector definition
		# because they are defined after the annotation source


class Annotation():
	def __init__(self, subj: Entity, rel: Event, annotation_source: AnnotationSource, relation_name: str):
		self.subj: Entity = subj
		self.rel: Event = rel
		self.relation_name: str = relation_name  # e.g., "place of birth"
		self.uri: str = None
		self.label: str = None
		self.document_name: str = annotation_source.label
		self.annotation_target = annotation_source.uri

		self.set_uri()
		self.set_label()

	def set_uri(self):
		if self.subj.uri and self.relation_name:
			self.uri = f'{self.subj.uri.replace(wikidata(""), temp_lincs(""))}/annotation/{self.relation_name.replace(" ", "_")}'

	def set_label(self):
		if self.subj.name and self.relation_name:
			self.label = f"Annotation of {self.document_name} for {self.relation_name}"
		else:
			self.label = ""

	def declare_annotation(self, g):
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E33_Linguistic_Object")))
		g.add(URIRef(self.uri), RDF.type, URIRef(oa("Annotation")))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.label, lang="en"))

		g.add(URIRef(self.uri), URIRef(cidoc("P67_refers_to")), URIRef(self.subj.uri))
		g.add(URIRef(self.uri), URIRef(cidoc("P67_refers_to")), URIRef(self.rel.obj_annotation_uri))

		g.add(URIRef(self.uri), URIRef(oa("hasBody")), URIRef(self.subj.uri))
		g.add(URIRef(self.uri), URIRef(oa("hasBody")), URIRef(self.rel.obj_annotation_uri))

		g.add(URIRef(self.uri), URIRef(oa("hasTarget")), URIRef(self.annotation_target))


class AttributionAssignment():
	def __init__(self, subj: Entity, obj: Entity, rel: Event, annotation: Annotation, relation_name: str, notes: list = []):
		self.subj: Entity = subj
		self.obj: Entity = obj
		self.rel: Event = rel
		self.annotation: Annotation = annotation
		self.relation_name: str = relation_name  # e.g., "place of birth"
		self.notes: list = notes  # the full sentence the relation came from and the value mentions
		self.uri: str = None
		self.label: str = None
		self.set_uri()
		self.set_label()

	def set_uri(self):
		if self.subj.uri and self.relation_name:
			self.uri = f'{self.subj.uri.replace(wikidata(""), temp_lincs(""))}/attributionAssignment/{self.relation_name.replace(" ", "_")}'
		else:
			self.uri = ""

	def set_label(self):
		if self.subj.name and self.relation_name:
			self.label = f"Attribution of {self.relation_name} to {self.subj.name}"
		else:
			self.label = ""

	def add_note(self, note):
		self.notes = self.notes.append(note)

	def declare_attribution_assignment(self, g):
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E13_Attribute_Assignment")))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.label, lang="en"))
		g.add(URIRef(self.uri), URIRef(cidoc("P141_assigned")), URIRef(self.obj.uri))
		g.add(URIRef(self.uri), URIRef(cidoc("P140_assigned_attribute_to")), URIRef(self.rel.uri))
		g.add(URIRef(self.uri), URIRef(cidoc("P129i_is_subject_of")), URIRef(self.annotation.uri))
		for note in self.notes:
			g.add(URIRef(self.uri), URIRef(cidoc("P3_has_note")), Literal(note, lang="en"))


class Selector():
	def __init__(self, document: Document, passage: str):
		self.uri: str = None
		self.label: str = "TODO figure out label"
		self.document: str = document.text
		self.document_name: str = document.name
		self.passage: str = passage
		self.start = None  # start index of passage in document
		self.end = None  # end index of passage in document
		self.set_uri()

	def set_uri(self):
		if self.start is None:
			self.start = self.document.find(self.passage)
			self.end = self.start + len(self.passage)

		self.uri = temp_lincs(f'Selector/{self.document_name.replace(" ", "_")}/{self.start}_{self.end}')

	def declare_generic_selector(self, g):
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E33_Linguistic_Object")))
		g.add(URIRef(self.uri), RDF.type, URIRef(oa("TextQuoteSelector")))
		g.add(URIRef(self.uri), RDFS.label, Literal(self.label, lang="en"))
		g.add(URIRef(self.uri), URIRef(oa("exact")), Literal(self.passage, lang="en"))


class SegmentSelector(Selector):
	def __init__(self, document: Document, passage: str, annotation_source: AnnotationSource):
		super(SegmentSelector, self).__init__(document, passage)
		self.annotation: AnnotationSource = annotation_source

	def declare_segment_selector(self, g):
		self.declare_generic_selector(g)
		g.add(URIRef(self.annotation.uri), URIRef(oa("hasSelector")), URIRef(self.uri))


class FragmentSelector(Selector):
	def __init__(self, document: Document, passage: str, segment_selector: Selector, start: str, end: str):
		super(FragmentSelector, self).__init__(document, passage)
		self.segment = segment_selector
		self.start = start  # start index of passage in document
		self.end = end  # end index of passage in document

	def declare_fragment_selector(self, g):
		self.declare_generic_selector(g)
		g.add(URIRef(self.segment.uri), URIRef(oa("refinedBy")), URIRef(self.uri))
