import rdflib
from rdflib import Namespace, URIRef, Literal
from rdflib.namespace import RDFS, RDF, XSD


class Graph():
	def __init__(self):
		self.graph = None
		self.create_graph()

	def create_graph(self):
		self.graph = rdflib.Graph()

		self.graph.bind("crm", Namespace("http://www.cidoc-crm.org/cidoc-crm/"))
		self.graph.bind("temp_lincs", Namespace("http://temp.lincsproject.ca/"))
		self.graph.bind("frbroo", Namespace("http://iflastandards.info/ns/fr/frbr/frbroo/"))
		self.graph.bind("lincs", Namespace("http://www.id.lincsproject.ca/"))
		self.graph.bind("wikidata", Namespace("http://www.wikidata.org/entity/"))
		self.graph.bind("rdf", Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#"))
		self.graph.bind("rdfs", Namespace("http://www.w3.org/2000/01/rdf-schema#"))
		self.graph.bind("owl", Namespace("http://www.w3.org/2002/07/owl#"))
		self.graph.bind("skos", Namespace("http://www.w3.org/2004/02/skos/core#"))
		self.graph.bind("oa", Namespace("http://www.w3.org/ns/oa#"))
		self.graph.bind("geonames", Namespace("https://sws.geonames.org/"))
		self.graph.bind("lexvo", Namespace("http://www.lexvo.org/id/iso639-3/"))

	def add(self, subj, pred, obj):
		self.graph.add((subj, pred, obj))

	def serialize(self, output_location):
		self.graph.serialize(destination=output_location)

	def to_string(self):
		return self.graph.serialize(format='turtle').encode('utf-8')
