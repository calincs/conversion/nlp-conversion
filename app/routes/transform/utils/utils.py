import urllib


def validate_url(text):
	return urllib.parse.quote(text)


def get_begin_end_dates(date_string):
	# TODO extract these properly and return None for both if can't be done reliably
	year = 1111
	month = 11
	day = 11
	begin = f"{year}-{month}-{day}T00:00:00"
	end = f"{year}-{month}-{day}T23:59:59"

	return begin, end


def text_after_last_period(text):
	# Find the last period in the text
	last_period_index = text.rfind('.')

	# If no period is found, return the whole string
	if last_period_index == -1:
		return text

	# If the last period is at the end of the text, return an empty string
	if last_period_index == len(text) - 1:
		return ""

	# Extract and return the text after the last period
	return text[last_period_index + 1:]


def text_up_to_next_period(text):
	# Split the text by periods
	sentences = text.split('.')

	return sentences[0] + "."
