import urllib.parse


def cidoc(text):
	return f"http://www.cidoc-crm.org/cidoc-crm/{text}"


def crmdig(text):
	return f"http://www.ics.forth.gr/isl/CRMdig/{text}"


def frbroo(text):
	return f"http://iflastandards.info/ns/fr/frbr/frbroo{text}"


def lincs(text):
	return f"http://id.lincsproject.ca/{text}"


def oa(text):
	return f"http://www.w3.org/ns/oa#{text}"


def skos(text):
	return f"http://www.w3.org/2004/02/skos/core#{text}"


def temp_lincs(text):
	# percent encodes the text so it is valid as a temporary URI
	return f"http://temp.lincsproject.ca/{urllib.parse.quote(text.lower())}"


def wikidata(text):
	return f"http://www.wikidata.org/entity/{text}"
