# Diffbot types that still need handling


## need examples before deciding the type

```
    {
        "diffbot_property_name": "academic title",
        "diffbot_property_uri": "https://diffbot.com/entity/EBxWHiGsIN46txCzqDKlrjw",
        "rdf_type": ["TBD"]},
    {
```

```
    {
        "diffbot_property_name": "award",
        "diffbot_property_uri": "https://diffbot.com/entity/EgXqbw14CORSyFpZeczosiQ",
        "rdf_type": ["TBD"]},
    {
```

```
    {
        "diffbot_property_name": "fictional entity",
        "diffbot_property_uri": "https://diffbot.com/entity/EkekIF8t8MwiyUNoMrYfREA",
        "rdf_type": ["TBD"]},
    {
```

```
    {
        "diffbot_property_name": "technology",
        "diffbot_property_uri": "https://diffbot.com/entity/E-5jnlQEGOt2x8rdkkht1NQ",
        "rdf_type": ["TBD"]},
    {
```


## will only be handled when they are part of relations -- or maybe add as type if not in relation

```
    {
        "diffbot_property_name": "gender",
        "diffbot_property_uri": "https://diffbot.com/entity/E2AEFWsqdMmucPVIn4gC5pg",
        "rdf_type": ["TBD"]},
    {
```

```
    {
        "diffbot_property_name": "cause of death",
        "diffbot_property_uri": "https://diffbot.com/entity/EizOAKeqSOqGgZR09P66tBQ",
        "rdf_type": ["TBD"]},
    {
```