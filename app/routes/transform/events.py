from rdflib import URIRef, Literal
from rdflib.namespace import RDFS, RDF, XSD

from app.routes.transform.utils.prefixes import *
from app.routes.transform.utils.utils import *
from app.routes.transform.graphs import Graph
from app.routes.transform.entities import Entity


class Event():
	def __init__(self):
		self.subject: Entity = None
		self.uri: str = None

	def set_subject(self, subject):
		self.subject = subject


class BirthEvent(Event):
	def __init__(self):
		super(BirthEvent, self).__init__()
		self.place: Entity = None
		self.date: str = None
		self.date_begin: str = None
		self.date_end: str = None
		self.obj_annotation_uri: str = None

	def set_place(self, place):
		self.place = place

	def set_date(self, date):
		self.date = date
		# TODO parse the date to get date_begin and date_end

	def set_uri(self):
		if self.subject.uri:
			self.uri = f'{self.subject.uri.replace(wikidata(""), temp_lincs(""))}/birthEvent'

	def declare_birth(self, g):

		# there is a birth event
		self.set_uri()
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E67_Birth")))
		g.add(URIRef(self.uri), RDFS.label, Literal(f"Birth event of {self.subject.name}", lang="en"))

		# the person has a birth event
		g.add(URIRef(self.subject.uri), URIRef(cidoc("P98i_was_born")), URIRef(self.uri))

		if self.place:
			# the birth event has a location
			g.add(URIRef(self.uri), URIRef(cidoc("P7_took_place_at")), URIRef(self.place.uri))
			self.obj_annotation_uri = self.place.uri  # The annotation will connect to the place

		if self.date:
			# the birth event has a timespan
			timespan_uri = f'{self.uri}/timespan'
			self.obj_annotation_uri = timespan_uri  # The annotation will connect to the timespan
			g.add(URIRef(self.uri), URIRef(cidoc("P4_has_time-span")), URIRef(timespan_uri))

			# the timespan has date/time information and a label and a type
			g.add(URIRef(timespan_uri), URIRef(cidoc("P82_at_some_time_within")), Literal(self.date, lang="en"))
			g.add(URIRef(timespan_uri), RDFS.label, Literal(f"Birth date of {self.subject.name}", lang="en"))
			g.add(URIRef(timespan_uri), RDF.type, URIRef(cidoc("E52_Time-Span")))

			# get start and end dates from the date
			begin, end = get_begin_end_dates(self.date)
			begin = None  # TEMP
			end = None  # TEMP
			if begin and end:
				g.add(URIRef(timespan_uri), URIRef(cidoc("P82a_begin_of_the_begin")), Literal(begin, datatype=XSD.dateTime))
				g.add(URIRef(timespan_uri), URIRef(cidoc("P82b_end_of_the_end")), Literal(end, datatype=XSD.dateTime))


class DeathEvent(Event):
	def __init__(self):
		super(DeathEvent, self).__init__()
		self.place: Entity = None
		self.date: str = None
		self.date_begin: str = None
		self.date_end: str = None
		self.cause: Entity = None
		self.obj_annotation_uri: str = None

	def set_place(self, place):
		self.place = place

	def set_date(self, date):
		self.date = date

	def set_cause(self, cause):
		self.cause = cause

	def set_uri(self):
		if self.subject.uri:
			self.uri = f'{self.subject.uri.replace(wikidata(""), temp_lincs(""))}/deathEvent'

	def declare_death(self, g):

		# there is a death event
		self.set_uri()
		g.add(URIRef(self.uri), RDF.type, URIRef(cidoc("E69_Death")))
		g.add(URIRef(self.uri), RDFS.label, Literal(f"Death event of {self.subject.name}", lang="en"))

		# the person has a death event
		g.add(URIRef(self.subject.uri), URIRef(cidoc("P100i_died_in")), URIRef(self.uri))

		if self.place:
			# the death event has a location
			g.add(URIRef(self.uri), URIRef(cidoc("P7_took_place_at")), (URIRef(self.place.uri)))
			self.obj_annotation_uri = self.place.uri  # The annotation will connect to the place

		if self.date:
			# the death event has a timespan
			timespan_uri = f'{self.uri}/timespan'
			self.obj_annotation_uri = timespan_uri  # The annotation will connect to the timespan
			g.add(URIRef(self.uri), URIRef(cidoc("P4_has_time-span")), URIRef(timespan_uri))

			# the timespan has date/time information and a label and a type
			g.add(URIRef(timespan_uri), URIRef(cidoc("P82_at_some_time_within")), Literal(self.date, lang="en"))
			g.add(URIRef(timespan_uri), RDFS.label, Literal(f"Death date of {self.subject.name}", lang="en"))
			g.add(URIRef(timespan_uri), RDF.type, URIRef(cidoc("E52_Time-Span")))

			# get start and end dates from the date
			begin, end = get_begin_end_dates(self.date)
			begin = None  # TEMP
			end = None  # TEMP
			if begin and end:
				g.add(URIRef(timespan_uri), URIRef(cidoc("P82a_begin_of_the_begin")), Literal(begin, datatype=XSD.dateTime))
				g.add(URIRef(timespan_uri), URIRef(cidoc("P82b_end_of_the_end")), Literal(end, datatype=XSD.dateTime))

		if self.cause:
			# a death event has an attribution assignment
			attribution_uri = f'{self.uri}/causeAttribution/{self.cause.name.replace(" ", "_")}'
			self.obj_annotation_uri = attribution_uri
			g.add(URIRef(self.uri), URIRef(cidoc("P140i_was_attributed_by")), URIRef(attribution_uri))

			# the attribution assignment has a rdf:type, label, and a cause
			g.add(URIRef(attribution_uri), RDF.type, URIRef(cidoc("E13_Attribute_Assignment")))
			g.add(URIRef(attribution_uri), RDFS.label, Literal(f"Cause of death event of {self.subject.name}", lang="en"))
			g.add(URIRef(attribution_uri), URIRef(cidoc("P141_assigned")), URIRef(self.cause.uri))

			# the cause is defined
			g.add(URIRef(self.cause.uri), RDF.type, URIRef(cidoc("E55_Type")))
			g.add(URIRef(self.cause.uri), RDFS.label, Literal(self.cause.name, lang="en"))
