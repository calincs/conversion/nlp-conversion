from flask import Flask, request, send_file, render_template
import io
import json

from app.routes.extract.request import nlp_api_request
from app.routes.transform.main import transform_json


def convert(text_file_content, json_file_content, document_title, document_url, document_language):
    converted_content = f"""
    <Document>
        <Title>{document_title}</Title>
        <URL>{document_url}</URL>
        <Content>{text_file_content.decode('utf-8')}</Content>
        <json_Content>{json_file_content.decode('utf-8')}</json_Content>
    </Document>
    """

    # Return the converted content as bytes
    return converted_content.encode('utf-8')


app = Flask(__name__)


@app.route('/extract', methods=['GET', 'POST'])
def extract():
    if request.method == 'POST':
        # Get form data
        file = request.files.get('fileInput')

        if file:
            # Process the uploaded file
            file_content = file.read().decode('utf-8')
            document_title = request.form.get('documentTitle')
            diffbot_key = request.form.get('diffbotKey')


            # Call your convert function with the necessary parameters
            converted_file = nlp_api_request(file_content, diffbot_key)

            # Return the file as a response for download
            return send_file(
                io.BytesIO(converted_file),
                download_name=f'{document_title}_entity_extraction.json',
                as_attachment=True,
                mimetype='text/plain'
            )

    return render_template('extract.html')


@app.route('/transform', methods=['GET', 'POST'])
def transform():
    if request.method == 'POST':
        # Get form data
        text_file = request.files.get('textFileInput')
        json_file = request.files.get('jsonFileInput')
        document_title = request.form.get('documentTitle')
        document_url = request.form.get('documentURL')
        document_language = request.form.get('docLanguage')

        if text_file:
            # Process the uploaded file
            text_file_content = text_file.read().decode('utf-8')

            if json_file:
                json_file_content = json.load(json_file)

                # Call your convert function with the necessary parameters
                converted_file = transform_json(text_file_content, json_file_content, document_title, document_url, document_language)

                # Return the file as a response for download
                return send_file(
                    io.BytesIO(converted_file),
                    download_name=f'{document_title}_entity_extraction.ttl',
                    as_attachment=True,
                    mimetype='text/plain'
                )

    return render_template('transform.html')


if __name__ == '__main__':
    app.run(debug=True)
