# Use an official Python runtime as a parent image
FROM python:3.9-slim

# Set the working directory in the container and copy app
WORKDIR /code
COPY ./requirements.txt /code/requirements.txt
COPY ./app /code/app

# Install the Python dependencies
RUN pip install --no-cache-dir --upgrade -r requirements.txt

# Expose the port the app runs on
EXPOSE 8012

# Define environment variables
ENV FLASK_APP=/code/app/app.py
ENV FLASK_APP=/code/app/app.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=8012
ENV FLASK_ENV=development

# Run the Flask app
CMD ["python", "-m", "flask", "run"]
